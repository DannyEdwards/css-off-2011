$(document).ready(function() {
  setInterval("timer()", 1000);
});

$('nav ul li a').click(function() {
  // Navigation click.
  var elementClicked = $(this).attr("href");
  var destination = $(elementClicked).offset().top;
  
  // Animate scroll.
  $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, 500 );
  
  return false;
});

$('.obstacle-switcher a').click(function() {
  // Get id.
  id = this.id.replace('-link','');
  
  // Switch the obstacle displayed.
  $('.obstacle-item').removeClass('active');
  $('.obstacle-switcher ul li').removeClass('active');
  
  $('#' + id + '-link').parent().addClass('active');
  $('#' + id + '-item').addClass('active');
  
  return false;
});

$('#submit').click(function() {
  // Let's validate the form.
  
  // Set up a couple of flags.
  var no_errors = true;
  var selects_invalid = 0;
  
  // Regular expressions.
  var email_regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var url_regex = /^((http|https|ftp):\/\/(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)$/;
  
  $('[name*="input"]').each( function(index, element) {
    value = $(element).val();
    name = $(element).attr('name').replace('input[', '').replace(']', '');
    
    // Go through each individual input type.
    if( name == 'email' ) {
      if ( email_regex.test(value) ) {
        fieldValid(element);
      }
      else {
        fieldInvalid(element);
        no_errors = false;
      }
    }
        
    else if( name == 'photo' ) {
      if ( url_regex.test(value) ) {
        fieldValid(element);
      }
      else {
        fieldInvalid(element);
        no_errors = false;
      }
    }
        
    else {
      if( value == '' || value == null ) {
        fieldInvalid(element);
        if ( name == 'gender' || name == 'team-colour') { selects_invalid++; }
        no_errors = false;
      }
      else {
        fieldValid(element);
      }
    }
    
  });
  
  // Sort out select arrow, only one has to be incorrect to display.
  if( selects_invalid > 0 ) {
    $('#select-arrow').addClass('error');
    $('#select-arrow').attr('alt', 'Invalid selection');
  }
  else  {
    $('#select-arrow').removeClass('error');
    $('#select-arrow').attr('alt', 'No error');
  }
  
  if( no_errors ) {
    submitData();
  }
  
  return false;
});

function fieldValid(element) {
  name = $(element).attr('name').replace('input[', '').replace(']', '');
  
  $(element).removeClass('error');
  $('#'+name+'-arrow').removeClass('error');
  $('#'+name+'-arrow').attr('alt','No error');
}

function fieldInvalid(element) {
  name = $(element).attr('name').replace('input[', '').replace(']', '');
  
  $(element).addClass('error');
  $('#'+name+'-arrow').addClass('error');
  $('#'+name+'-arrow').attr('alt','Invalid field');
}

function submitData() {
	// Submit the data.
	$('#submit').addClass('hidden');
	$('#submit-loader').removeClass('hidden');
	
	// Ajax would go here.
  setTimeout(function(){
    // Let's do a fake little loading thing to simulate an Ajax request...
    $('#submit-loader').addClass('hidden');
    $('#submit').removeClass('hidden');
    $('#submit').attr('disabled', 'disabled');
    $('#submit').val('Details Sent');
    
  }, 3000);
  
	return false;
}

function timer() {
  // Decrement the timer.
  var second = $('#time').html();
  second--;
  
  if( second >= 0 ) {
    $('#time').html(second);
  }
}